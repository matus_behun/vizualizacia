cmake_minimum_required(VERSION 2.8)
 
PROJECT(PlochoGen)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}")
find_package(VTK REQUIRED)
include(${VTK_USE_FILE})

if (MSVC)
add_definitions( "/D_CRT_SECURE_NO_WARNINGS /wd4996 /wd4244 /wd4267 /wd4334 /MP /bigobj" )
endif(MSVC)


find_package(Qt5Widgets REQUIRED QUIET)
  include_directories(${Qt5Widgets_INCLUDE_DIRS})
  add_definitions(${Qt5Widgets_DEFINITIONS})
  set(QT_LIBRARIES ${Qt5Widgets_LIBRARIES})
find_package(Qt5Concurrent REQUIRED)
        list(APPEND QT_LIBRARIES ${Qt5Concurrent_LIBRARIES} )

if(UNIX AND NOT APPLE)
find_package(Qt5X11Extras REQUIRED)
        list(APPEND QT_LIBRARIES ${Qt5X11Extras_LIBRARIES} )
endif(UNIX AND NOT APPLE)
  
include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR}) 

file(GLOB UI_FILES src/*.ui)
file(GLOB_RECURSE QT_WRAP src/*.h)
file(GLOB_RECURSE VTKAPP_SOURCE
                  src/*.h
                  src/*.cpp
        )
QT5_WRAP_UI(UISrcs ${UI_FILES})
QT5_WRAP_CPP(MOCSrcs ${QT_WRAP})
add_executable(PlochoGen MACOSX_BUNDLE ${VTKAPP_SOURCE} ${UISrcs} ${MOCSrcs})
 
target_link_libraries(PlochoGen ${VTK_LIBRARIES} ${QT_LIBRARIES})
