﻿#include "transformdialog.h"
#include "ui_transformdialog.h"

TransformDialog::TransformDialog(QWidget * parent, std::vector<int> *selPoints) : QDialog(parent) {
	ui = new Ui::TransformDialog();
	ui->setupUi(this); 
	this->selPoints = selPoints;
	ui->label_4->setToolTip("0 or empty for no coloring\ninsert text \"color\" for single color selected from colordialog\ninsert mathematic function with x,y and z as current point coordinates to calculate custom color");
	ui->colorLine->setToolTip("0 or empty for no coloring\ninsert text \"color\" for single color selected from colordialog\ninsert mathematic function with x,y and z as current point coordinates to calculate custom color");
}

TransformDialog::~TransformDialog() {
	delete ui;
	if (xcompleter != nullptr) delete xcompleter;
	if (ycompleter != nullptr) delete ycompleter;
	if (zcompleter != nullptr) delete zcompleter;
	if (colcompleter != nullptr) delete colcompleter;
}

void TransformDialog::ApplyButtonClicked()
{
	this->ui->cteWidget->GetInfo();
	emit apply_clicked();
}

bool TransformDialog::Transform(vtkSmartPointer<vtkPolyData> polydata)
{
	std::string expression_string1 = ui->xLine->text().toStdString();
	std::string expression_string2 = ui->yLine->text().toStdString();
	std::string expression_string3 = ui->zLine->text().toStdString();
	std::string expression_string4 = ui->colorLine->text().toStdString();
	coloring colMode = NO_COLOR;
	if (ui->colorLine->text().isEmpty() || ui->colorLine->text() == "0") colMode = NO_COLOR;
	else
	{
		if (ui->colorLine->text() == "color") colMode = SINGLE_COLOR;
		else colMode = FUNCTION_COLOR;
	}
	double bod[3];
	symbol_table_t symbol_table;
	symbol_table.add_variable("x", bod[0]);
	symbol_table.add_variable("y", bod[1]);
	symbol_table.add_variable("z", bod[2]);
	symbol_table.add_constants();
	expression_t expression1;
	expression1.register_symbol_table(symbol_table);
	expression_t expression2;
	expression2.register_symbol_table(symbol_table);
	expression_t expression3;
	expression3.register_symbol_table(symbol_table);
	expression_t expression4;
	expression4.register_symbol_table(symbol_table);

	parser_t parser;
	if (!parser.compile(expression_string1, expression1)) {
		std::cout << parser.error() << std::endl;
		return false;
	}
	if (!parser.compile(expression_string2, expression2)){
		std::cout << parser.error() << std::endl;
		return false;
	}
	if (!parser.compile(expression_string3, expression3)){
		std::cout << parser.error() << std::endl;
		return false;
	}
	if (colMode == FUNCTION_COLOR)
	{
		if (!parser.compile(expression_string4, expression4)) {
			std::cout << parser.error() << std::endl;
			return false;
		}
	}
	std::vector<double> hodnoty;
	vtkSmartPointer<vtkColorTransferFunction> color = this->ui->cteWidget->GetColorFunction();
	
	vtkSmartPointer<vtkUnsignedCharArray> colors;
	size_t pocet = polydata->GetNumberOfPoints();
	if (selPoints->size() == 0)
	{
		if (colMode == FUNCTION_COLOR)
		{
			hodnoty.resize(pocet);
			min = 999999;
			max = -999999;
		}
		for (int i = 0; i < pocet; i++)
		{
			polydata->GetPoint(i, bod);
			double x = expression1.value();
			double y = expression2.value();
			double z = expression3.value();
			polydata->GetPoints()->SetPoint(i, x, y, z);

			if (colMode == FUNCTION_COLOR)
			{
				hodnoty[i] = expression4.value();
				if (hodnoty[i] > max)max = hodnoty[i];
				if (hodnoty[i] < min)min = hodnoty[i];
			}
			
			
			//std::cout << bod[0] << " " << bod[1] << " " << bod[2] << std::endl;
		}
		if (colMode != NO_COLOR)
		{
			std::cout << min << " " << max << std::endl;
			
			if (polydata->GetPointData()->GetScalars("Colors") != nullptr)
			{
				colors = vtkUnsignedCharArray::SafeDownCast(polydata->GetPointData()->GetScalars("Colors"));
			}
			else
			{
				colors = vtkSmartPointer<vtkUnsignedCharArray>::New();
				colors->SetNumberOfComponents(3);
				colors->SetName("Colors");
				colors->SetNumberOfTuples(pocet);
			}

			unsigned char farba[3];
			double rgb[3];
			if (colMode == FUNCTION_COLOR)
			{
				for (int i = 0; i < pocet; i++)
				{
					color->GetColor((hodnoty[i] - min) / (max - min), rgb);
					farba[0] = rgb[0] * 255;
					farba[1] = rgb[1] * 255;
					farba[2] = rgb[2] * 255;
					colors->SetTypedTuple(i, farba);
				}
			}
			if (colMode == SINGLE_COLOR)
			{
				QColor qc = QColorDialog::getColor(Qt::white, this, "Select color");
				if (!qc.isValid())qc = Qt::white;
				farba[0] = qc.red();
				farba[1] = qc.green();
				farba[2] = qc.blue();
				for (int i = 0; i < pocet; i++)
				{
					colors->SetTypedTuple(i, farba);
				}
			}
			polydata->GetPointData()->SetScalars(colors);
		}
		polydata->Modified();
	}
	else
	{
		bool skalars = false;
		if (colMode != NO_COLOR)
		{
			if (polydata->GetPointData()->GetScalars("Colors") != nullptr)
			{
				if (colMode == FUNCTION_COLOR)hodnoty.resize(selPoints->size());
				colors = vtkUnsignedCharArray::SafeDownCast(polydata->GetPointData()->GetScalars("Colors"));
				skalars = true;
			}
		}
		for (int i = 0; i < selPoints->size(); i++)
		{
			polydata->GetPoint((*selPoints)[i], bod);
			double x = expression1.value();
			double y = expression2.value();
			double z = expression3.value();
			polydata->GetPoints()->SetPoint((*selPoints)[i], x, y, z);

			if (colMode== FUNCTION_COLOR && skalars)
			{
				hodnoty[i] = expression4.value();
				if (hodnoty[i] > max)max = hodnoty[i];
				if (hodnoty[i] < min)min = hodnoty[i];
			}
			//std::cout << bod[0] << " " << bod[1] << " " << bod[2] << std::endl;
		}
		if (colMode != NO_COLOR && skalars)
		{
			std::cout << min << " " << max << std::endl;
			unsigned char farba[3];
			double rgb[3];
			if (colMode == FUNCTION_COLOR)
			{
				for (int i = 0; i < hodnoty.size(); i++)
				{
					color->GetColor((hodnoty[i] - min) / (max - min), rgb);
					farba[0] = rgb[0] * 255;
					farba[1] = rgb[1] * 255;
					farba[2] = rgb[2] * 255;
					colors->SetTypedTuple((*selPoints)[i], farba);
				}
			}
			if (colMode == SINGLE_COLOR)
			{
				QColor qc = QColorDialog::getColor(Qt::white, this, "Select color");
				if (!qc.isValid())qc = Qt::white;
				farba[0] = qc.red();
				farba[1] = qc.green();
				farba[2] = qc.blue();
				for (int i = 0; i < selPoints->size(); i++)
				{
					colors->SetTypedTuple((*selPoints)[i], farba);
				}
			}
			polydata->GetPointData()->SetScalars(colors);		
		}
		polydata->Modified();
	}

	if (xcompleter != nullptr)
	{
		if (!xcomp->contains(ui->xLine->text(), Qt::CaseInsensitive))*xcomp << ui->xLine->text();
		((QStringListModel*)xcompleter->model())->setStringList(*xcomp);
	}
	if (ycompleter != nullptr)
	{
		if (!ycomp->contains(ui->yLine->text(), Qt::CaseInsensitive))*ycomp << ui->yLine->text();
		((QStringListModel*)ycompleter->model())->setStringList(*ycomp);
	}
	if (zcompleter != nullptr)
	{
		if (!zcomp->contains(ui->zLine->text(), Qt::CaseInsensitive))*zcomp << ui->zLine->text();
		((QStringListModel*)zcompleter->model())->setStringList(*zcomp);
	}
	if (colcompleter != nullptr)
	{
		if (!colcomp->contains(ui->colorLine->text(), Qt::CaseInsensitive))*colcomp << ui->colorLine->text();
		((QStringListModel*)colcompleter->model())->setStringList(*colcomp);
	}

	return true;
}

void TransformDialog::SetAutoComplete(QStringList * xcomp, QStringList * ycomp, QStringList * zcomp, QStringList * colcomp)
{
	this->xcomp = xcomp;
	this->ycomp = ycomp;
	this->zcomp = zcomp;
	this->colcomp = colcomp;
	xcompleter = new QCompleter(*xcomp, this);
	xcompleter->setCaseSensitivity(Qt::CaseInsensitive);
	xcompleter->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
	xcompleter->setFilterMode(Qt::MatchContains);
	ui->xLine->setCompleter(xcompleter);

	ycompleter = new QCompleter(*ycomp, this);
	ycompleter->setCaseSensitivity(Qt::CaseInsensitive);
	ycompleter->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
	ycompleter->setFilterMode(Qt::MatchContains);
	ui->yLine->setCompleter(ycompleter);

	zcompleter = new QCompleter(*zcomp, this);
	zcompleter->setCaseSensitivity(Qt::CaseInsensitive);
	zcompleter->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
	zcompleter->setFilterMode(Qt::MatchContains);
	ui->zLine->setCompleter(zcompleter);

	colcompleter = new QCompleter(*colcomp, this);
	colcompleter->setCaseSensitivity(Qt::CaseInsensitive);
	colcompleter->setCompletionMode(QCompleter::UnfilteredPopupCompletion);
	colcompleter->setFilterMode(Qt::MatchContains);
	ui->colorLine->setCompleter(colcompleter);
}

