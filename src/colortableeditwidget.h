#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>
#include <iostream>

#include <vtkColorTransferFunction.h>
#include <vtkSmartPointer.h>


class ColorTableEditWidget : public QWidget
{
	Q_OBJECT

public:
	ColorTableEditWidget(QWidget *parent = 0);

	bool newImage(int x, int y);
	void GetInfo();
	vtkSmartPointer<vtkColorTransferFunction> GetColorFunction();


protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);

private:
	void resizeImage(QImage &image, const QSize &newSize);
	void DrawScale();

	QImage image;
	QPoint lastPoint;
	vtkSmartPointer<vtkColorTransferFunction> color;
};

#endif // PAINTWIDGET_H
