#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <iostream>
#include <random>
#include <cmath>
#include <chrono>
#include <vector>
#include <array>
#include <thread>

// Qt
#include <QMainWindow>
#include <QtCore>
#include <QFileDialog>
#include <QWindow>
#include <QProgressDialog>
#include <QFuture>
#include <QMessageBox>
#include <QtConcurrent>

// Visualization Toolkit (VTK)
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkProperty.h>
#include <vtkAxesActor.h>
#include <vtkOrientationMarkerWidget.h>
#include <vtkPolyDataNormals.h>
#include <vtkDataSetMapper.h>
#include <vtkLinearSubdivisionFilter.h>
#include <vtkLoopSubdivisionFilter.h>
#include <vtkButterflySubdivisionFilter.h>
#include <vtkDecimatePro.h>
#include <vtkQuadricDecimation.h>
#include <vtkQuadricClustering.h>
#include <vtkSmoothPolyDataFilter.h>
#include <vtkWindowedSincPolyDataFilter.h>
#include <vtkColorTransferFunction.h>
#include <vtkPointData.h>
#include <vtkFloatArray.h>

#include "torusdialog.h"
#include "transformdialog.h"
#include "planedialog.h"
#include "spheredialog.h"
#include "plochointeractorstyle.h"
#include "tvarodialog.h"
#include "implicitdialog.h"


namespace Ui
{
  class PlochoUI;
}

class PlochoUI : public QMainWindow
{
	Q_OBJECT

public:
	explicit PlochoUI(QWidget *parent = 0);
	~PlochoUI();
	void show();

	public slots:
	void openFile();
	void saveFile();
	void reset_viewport_clicked();
	void exit_clicked();
	void UpdateWidget();
	void create_torus();
	void create_plane();
	void create_sphere();
	void create_implicit();
	void transform();
	void ApplyTransform();
	void LinearDiv();
	void LoopDiv();
	void ButterDiv();
	void DecPro();
	void DecQuad();
	void DecClus();
	void SmoothLap();
	void SmoothWin();

protected:
	void  closeEvent(QCloseEvent*);

private:
	void ReadAutoComplete();
	void SaveAutoComplete();
	enum tvar
	{
		torus,
		sphere,
		plane,
		implicit,
		fromFile	
	};
	QSettings settings;
	QSettings inisettings;
	Ui::PlochoUI *ui;
	bool computeNormals = true;
	vtkSmartPointer<vtkRenderer> renderer;
	vtkSmartPointer<vtkOrientationMarkerWidget> widget;
	vtkSmartPointer<vtkAxesActor> axes;
	vtkSmartPointer<vtkActor> actor;
	vtkSmartPointer<vtkPolyData> polydata;
	vtkSmartPointer<PlochoInteractorStyle> plochoInteractorStyle;
	TransformDialog transDialog;
	TorusDialog tdialog;
	PlaneDialog pdialog;
	SphereDialog sdialog;
	ImplicitDialog idialog;
	void Subdivide(int which);
	void Decimate(int which);
	void Smooth(int which);
	void create_object(tvar which);
	vtkSmartPointer<vtkPolyDataNormals> normalGenerator;
	void CalculateNormals();
	std::vector<int> selPoints;
	QStringList xcomp, ycomp, zcomp, colcomp;
	

};

#endif // MAINWINDOW_H
