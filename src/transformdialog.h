﻿#pragma once
#ifndef TRANSFORMDIALOG_HPP
#define TRANSFORMDIALOG_HPP
#include <QDialog>

#include <vtkSmartPointer.h>
#include <vtkPolyData.h>
#include <vtkColorTransferFunction.h>
#include <vtkPointData.h>
#include <QCompleter>
#include <QStringListModel>
#include <QMessageBox>
#include <QColorDialog>


#include "exprtk.hpp"
#include "colortableeditwidget.h"

typedef exprtk::symbol_table<double> symbol_table_t;
typedef exprtk::expression<double>     expression_t;
typedef exprtk::parser<double>             parser_t;


namespace Ui {class TransformDialog;}

class TransformDialog : public QDialog {
	Q_OBJECT

public:
	TransformDialog(QWidget * parent = Q_NULLPTR, std::vector<int> *selPoints=NULL);
	~TransformDialog();
	bool Transform(vtkSmartPointer<vtkPolyData> polydata);
	void SetAutoComplete(QStringList *xcomp, QStringList *ycomp, QStringList *zcomp, QStringList *colcomp);

public slots:
	void ApplyButtonClicked();

signals:
    void apply_clicked();

private:
	Ui::TransformDialog *ui;
	std::vector<int> *selPoints;
	double min = 999999;
	double max = -999999; 
	QStringList *xcomp = nullptr, *ycomp = nullptr, *zcomp = nullptr, *colcomp = nullptr;
	QCompleter *xcompleter = nullptr, *ycompleter = nullptr, *zcompleter = nullptr, *colcompleter = nullptr;
	enum coloring
	{
		NO_COLOR,
		SINGLE_COLOR,
		FUNCTION_COLOR
	};
};

#endif // TRANSFORMDIALOG_HPP