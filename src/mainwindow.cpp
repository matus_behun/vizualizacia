﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

//if(x>10,-cos((sqrt(y*y+z*z)/40)*pi)*5+5,if(x<-10,cos((-sqrt(y*y+z*z)/40)*pi)*5-5,x))
//if(x>10,-cos((sqrt(y*y+z*z)/40)*pi*2)*5+5,if(x<-10,cos((-sqrt(y*y+z*z)/40)*pi*2)*5-5,0))
//if(x>10,-cos((sqrt(y*y+z*z)/40)*pi*1.7)*5+5,if(x<-10,cos((-sqrt(y*y+z*z)/40)*pi*1.7)*5-5,x/3.8))

PlochoUI::PlochoUI(QWidget *parent) :
	QMainWindow(parent), transDialog(this,&selPoints), inisettings(QSettings::IniFormat,QSettings::UserScope,"SvF STU","PlochoGen"),
	ui(new Ui::PlochoUI)
{
	ui->setupUi(this);
	this->setWindowTitle("PlochoGen");

	QAction *helpAction = ui->menuBar->addAction("Transform");
	connect(helpAction, SIGNAL(triggered()), this, SLOT(transform()));

	connect(&transDialog, &TransformDialog::apply_clicked, this, &PlochoUI::ApplyTransform);
	vtkObject::GlobalWarningDisplayOff();

	//incializacia vtk rendera
	renderer = vtkSmartPointer<vtkRenderer>::New();
	QSurfaceFormat surfaceFormat = ui->qvtkWidget->windowHandle()->format();
	surfaceFormat.setSamples(8);
	surfaceFormat.setStencilBufferSize(8);
	ui->qvtkWidget->windowHandle()->setFormat(surfaceFormat);

	ui->qvtkWidget->GetRenderWindow()->AddRenderer(renderer);

	plochoInteractorStyle = vtkSmartPointer<PlochoInteractorStyle>::New();
	ui->qvtkWidget->GetRenderWindow()->GetInteractor()->SetInteractorStyle(plochoInteractorStyle);
	plochoInteractorStyle->init(renderer, &selPoints);
	ui->qvtkWidget->GetRenderWindow()->SetMultiSamples(8);

	renderer->ResetCamera();
	renderer->SetBackground(0.2, 0.3, 0.4);

	axes = vtkSmartPointer<vtkAxesActor>::New();
	widget = vtkSmartPointer<vtkOrientationMarkerWidget>::New();
	widget->SetOrientationMarker(axes);
	widget->SetInteractor(ui->qvtkWidget->GetRenderWindow()->GetInteractor());
	widget->SetViewport(0.0, 0.0, 0.1, 0.2);
	widget->SetEnabled(1);
	widget->InteractiveOff();
	ui->qvtkWidget->update();
	connect(plochoInteractorStyle.GetPointer(), SIGNAL(UpdateWidget()), this, SLOT(UpdateWidget()));
	ReadAutoComplete();

}
//destruktor
PlochoUI::~PlochoUI()
{
	//ulozime si polohu a velkost okna
	if (!this->isMaximized())
	{
		settings.setValue("window_width", this->width());
		settings.setValue("window_height", this->height());
		settings.setValue("window_posX", this->pos().x());
		settings.setValue("window_posY", this->pos().y());
	}
	
	settings.setValue("maximized", this->isMaximized());
	settings.sync();
	SaveAutoComplete();
    delete ui;
}

//vola sa, ked sa ma zobrazit hlavne okno
void PlochoUI::show()
{
	QWidget::show();
	//nastavime velkost a polohu okna podla ulozenych hodnot z minula
	this->resize(QSize(settings.value("window_width", this->width()).toInt(), settings.value("window_height", this->height()).toInt()));
	this->move(settings.value("window_posX", this->pos().x()).toInt(), settings.value("window_posY", this->pos().y()).toInt());
	if (settings.value("maximized", false).toBool())this->showMaximized();
}

void PlochoUI::saveFile()
{
	if (!polydata) {
		return;
	}

	QString nazovSuboru = QFileDialog::getSaveFileName(this, "Save file", "", tr("Data (*.vtk)"));

	if (nazovSuboru.isEmpty()) {
		return;
	}

	vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();

	writer->SetFileName(nazovSuboru.toStdString().c_str());

#if VTK_MAJOR_VERSION <= 5
	writer->SetInput(polydata);
#else
	writer->SetInputData(polydata);
#endif

	writer->Write();
}

void PlochoUI::openFile()
{
	QString nazovSuboru = QFileDialog::getOpenFileName(this, u8"Open file", "", tr("Data (*.vtk)"));

	if (nazovSuboru.isEmpty()) {
		return;
	}

	vtkSmartPointer<vtkPolyDataReader> reader = vtkSmartPointer<vtkPolyDataReader>::New();
	reader->ReadFromInputStringOff();
	reader->SetFileName(nazovSuboru.toStdString().c_str());

	if (!reader->IsFilePolyData()) {
		QMessageBox upozornenie;
		upozornenie.setText("File not in vtkPolyData format");
		upozornenie.exec();
		return;
	}

	reader->Update();
	polydata = reader->GetOutput();

	vtkSmartPointer<vtkPolyDataMapper> map = vtkSmartPointer<vtkPolyDataMapper>::New();
	map->SetInputConnection(reader->GetOutputPort());
	
	actor = vtkSmartPointer<vtkActor>::New();
	actor->SetMapper(map);

	renderer->AddActor(actor);
	renderer->ResetCamera();
	plochoInteractorStyle->ClearPoints();
	this->UpdateWidget();

	std::cout << "Total number of points: " << polydata->GetNumberOfPoints() << std::endl;

}

//resetovanie kamery zobrazenia
void PlochoUI::reset_viewport_clicked()
{
	this->renderer->ResetCamera();
	this->ui->qvtkWidget->update();
}

void   PlochoUI::closeEvent(QCloseEvent*)
{
	qApp->quit();
}

void PlochoUI::ReadAutoComplete()
{
	int size = inisettings.beginReadArray("xcomp");
	for (int i = 0; i < size; ++i)
	{
		inisettings.setArrayIndex(i);
		xcomp << inisettings.value("word").toString();
	}
	inisettings.endArray();
	size = inisettings.beginReadArray("ycomp");
	for (int i = 0; i < size; ++i)
	{
		inisettings.setArrayIndex(i);
		ycomp << inisettings.value("word").toString();
	}
	inisettings.endArray();
	size = inisettings.beginReadArray("zcomp");
	for (int i = 0; i < size; ++i)
	{
		inisettings.setArrayIndex(i);
		zcomp << inisettings.value("word").toString();
	}
	inisettings.endArray();
	size = inisettings.beginReadArray("colcomp");
	for (int i = 0; i < size; ++i)
	{
		inisettings.setArrayIndex(i);
		colcomp << inisettings.value("word").toString();
	}
	inisettings.endArray();
	transDialog.SetAutoComplete(&xcomp, &ycomp, &zcomp, &colcomp);
}

void PlochoUI::SaveAutoComplete()
{
	inisettings.beginWriteArray("xcomp");
	for (int i = 0; i < xcomp.size(); ++i) {
		if (xcomp.at(i).isEmpty())continue;
		inisettings.setArrayIndex(i);
		inisettings.setValue("word", xcomp.at(i));
	}
	inisettings.endArray();

	inisettings.beginWriteArray("ycomp");
	for (int i = 0; i < ycomp.size(); ++i) {
		if (ycomp.at(i).isEmpty())continue;
		inisettings.setArrayIndex(i);
		inisettings.setValue("word", ycomp.at(i));
	}
	inisettings.endArray();

	inisettings.beginWriteArray("zcomp");
	for (int i = 0; i < zcomp.size(); ++i) {
		if (zcomp.at(i).isEmpty())continue;
		inisettings.setArrayIndex(i);
		inisettings.setValue("word", zcomp.at(i));
	}
	inisettings.endArray();

	inisettings.beginWriteArray("colcomp");
	for (int i = 0; i < colcomp.size(); ++i) {
		if (colcomp.at(i).isEmpty())continue;
		inisettings.setArrayIndex(i);
		inisettings.setValue("word", colcomp.at(i));
	}
	inisettings.endArray();
	inisettings.sync();
}

//ukoncenie
void PlochoUI::exit_clicked()
{
	this->close();
}

void PlochoUI::UpdateWidget()
{
	this->ui->qvtkWidget->update();
}

void PlochoUI::create_torus()
{
	create_object(tvar::torus);
}

void PlochoUI::create_plane()
{
	create_object(tvar::plane);
}

void PlochoUI::create_sphere()
{
	create_object(tvar::sphere);
}

void PlochoUI::create_implicit()
{
	create_object(tvar::implicit);
}

void PlochoUI::create_object(tvar which)
{
	TvaroDialog *tvardialog;
	switch (which)
	{
	case tvar::plane:
		tvardialog = &pdialog;
		break;
	case tvar::sphere:
		tvardialog = &sdialog;
		break;
	case tvar::torus:
		tvardialog = &tdialog;
		break;
	case tvar::implicit:
		tvardialog = &idialog;
		break;
	default:
		return;
	}
	if (tvardialog->exec() == QDialog::Rejected)return;
	renderer->RemoveAllViewProps();

	//pocas generovania zobrazime dialog s progressbarom aby pouzivatel nemal zamrznute okno
	QProgressDialog progress(this);
	progress.setWindowModality(Qt::WindowModal);
	progress.setWindowFlags(progress.windowFlags() & ~Qt::WindowContextHelpButtonHint & ~Qt::WindowCloseButtonHint);
	progress.setLabelText("Generating surface...");
	progress.setCancelButton(0);
	progress.setRange(0, 0);
	progress.setMinimumDuration(0);
	progress.show();
	//pustime funkciu GetActor na pozadi pomocou QtConcurrent::run
	QFuture<vtkSmartPointer<vtkActor>> future = QtConcurrent::run(std::bind(&TvaroDialog::GetActor, tvardialog));
	//v cykle cakame kym neskonci beh funkcie
	while (true)
	{
		if (!future.isFinished())
		{
			if (!progress.isVisible())
				progress.show();
			//aplikacia spracuvava udalosti, aby nebola zamrznuta
			QApplication::processEvents();
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
		}
		else break;
	}
	progress.cancel();

	//ziskame vysledok z QFuture
	actor = future.result();
	if (actor == nullptr)return;
	polydata = vtkPolyData::SafeDownCast(actor->GetMapper()->GetInput());
	renderer->AddActor(actor);
	renderer->ResetCamera();
	plochoInteractorStyle->ClearPoints();
	tvardialog->ClearActor();
	this->UpdateWidget();
	std::cout << "Total number of points: " << polydata->GetNumberOfPoints() << std::endl;
}

void PlochoUI::transform()
{
	if (actor == nullptr)return;
	transDialog.show();
	transDialog.raise();
	transDialog.activateWindow();
}

void PlochoUI::ApplyTransform()
{
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	std::cout << "Total number of points: " << polydata->GetNumberOfPoints() << std::endl;
	if (transDialog.Transform(polydata) == false)return;
	polydata->Modified();
	CalculateNormals();
	renderer->ResetCamera();
	this->UpdateWidget();
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

	std::cout << "It took me " << time_span.count() << " seconds." << std::endl;
}

void PlochoUI::LinearDiv()
{
	Subdivide(0);
}

void PlochoUI::LoopDiv()
{
	Subdivide(1);
}

void PlochoUI::ButterDiv()
{
	Subdivide(2);
}

void PlochoUI::DecPro()
{
	Decimate(0);
}

void PlochoUI::DecQuad()
{
	Decimate(1);
}

void PlochoUI::DecClus()
{
	Decimate(2);
}

void PlochoUI::SmoothLap()
{
	Smooth(0);
}

void PlochoUI::SmoothWin()
{
	Smooth(1);
}

void PlochoUI::Decimate(int which)
{
	if (actor == nullptr)return;
	vtkSmartPointer<vtkPolyDataAlgorithm> decimationFilter;
	switch (which)
	{
	case 0:
		decimationFilter = vtkSmartPointer<vtkDecimatePro>::New();
		dynamic_cast<vtkDecimatePro *> (decimationFilter.GetPointer())->SetTargetReduction(0.5);
		break;
	case 1:
		decimationFilter = vtkSmartPointer<vtkQuadricDecimation>::New();
		break;
	case 2:
		decimationFilter = vtkSmartPointer<vtkQuadricClustering>::New();
		break;
	default:
		break;
	}


	decimationFilter->SetInputData(polydata);
	decimationFilter->Update();
	polydata->ShallowCopy(decimationFilter->GetOutput());
	polydata->Modified();
	CalculateNormals();
	this->UpdateWidget();
}

void PlochoUI::Smooth(int which)
{
	if (actor == nullptr)return;
	vtkSmartPointer<vtkPolyDataAlgorithm> smoothFilter;
	switch (which)
	{
	case 0:
		smoothFilter = vtkSmartPointer<vtkSmoothPolyDataFilter>::New();
		break;
	case 1:
		smoothFilter = vtkSmartPointer<vtkWindowedSincPolyDataFilter>::New();
		dynamic_cast<vtkWindowedSincPolyDataFilter *> (smoothFilter.GetPointer())->BoundarySmoothingOff();
		break;
	default:
		break;
	}
	smoothFilter->SetInputData(polydata);
	smoothFilter->Update();
	polydata->ShallowCopy(smoothFilter->GetOutput());
	polydata->Modified();
	CalculateNormals();
	this->UpdateWidget();
}

void PlochoUI::Subdivide(int which)
{
	if (actor == nullptr)return;
	vtkSmartPointer<vtkPolyDataAlgorithm> subdivisionFilter;
	switch (which)
	{
	case 0:
		subdivisionFilter = vtkSmartPointer<vtkLinearSubdivisionFilter>::New();
		dynamic_cast<vtkLinearSubdivisionFilter *> (subdivisionFilter.GetPointer())->SetNumberOfSubdivisions(1);
		break;
	case 1:
		subdivisionFilter = vtkSmartPointer<vtkLoopSubdivisionFilter>::New();
		dynamic_cast<vtkLoopSubdivisionFilter *> (subdivisionFilter.GetPointer())->SetNumberOfSubdivisions(1);
		break;
	case 2:
		subdivisionFilter = vtkSmartPointer<vtkButterflySubdivisionFilter>::New();
		dynamic_cast<vtkButterflySubdivisionFilter *> (subdivisionFilter.GetPointer())->SetNumberOfSubdivisions(1);
		break;
	default:
		break;
	}

	subdivisionFilter->SetInputData(polydata);
	subdivisionFilter->Update();
	polydata->ShallowCopy(subdivisionFilter->GetOutput());
	polydata->Modified();
	CalculateNormals();
	this->UpdateWidget();
}

//funckia na vypocet normal, pretoze vtk filter pre normaly je prilis pomaly
void PlochoUI::CalculateNormals()
{
	if (!computeNormals)
	{
		return;
	}
	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

	std::vector<std::array<float, 3>> normals(polydata->GetNumberOfPoints());
	vtkPoints *points = polydata->GetPoints();
	vtkSmartPointer<vtkFloatArray> pointNormalsArray = vtkSmartPointer<vtkFloatArray>::New();
	pointNormalsArray->SetNumberOfComponents(3); //3d normals (ie x,y,z)
	pointNormalsArray->SetNumberOfTuples(polydata->GetNumberOfPoints());
	double pole1[3], pole2[3], pole3[3];
	int index1;
	int index2;
	int index3;
	polydata->GetPolys()->InitTraversal();
	vtkIdType npts, *pts;
	while(polydata->GetPolys()->GetNextCell(npts, pts))
	{
		index1 = pts[0];
		index2 = pts[1];
		index3 = pts[2];

		points->GetPoint(index1, pole1);
		points->GetPoint(index2, pole2);
		points->GetPoint(index3, pole3);

		float side1[3];
		side1[0] = pole2[0] - pole1[0];
		side1[1] = pole2[1] - pole1[1];
		side1[2] = pole2[2] - pole1[2];
		float side2[3];
		side2[0] = pole3[0] - pole1[0];
		side2[1] = pole3[1] - pole1[1];
		side2[2] = pole3[2] - pole1[2];
		float norm[3];
		norm[0] = side1[1] * side2[2] - side1[2] * side2[1];
		norm[1] = side1[2] * side2[0] - side1[0] * side2[2];
		norm[2] = side1[0] * side2[1] - side1[1] * side2[0];
		float area = sqrt(norm[0] * norm[0] + norm[1] * norm[1] + norm[2] * norm[2])/2.0;

		normals[index1][0] += norm[0] * area;
		normals[index1][1] += norm[1] * area;
		normals[index1][2] += norm[2] * area;

		normals[index2][0] += norm[0] * area;
		normals[index2][1] += norm[1] * area;
		normals[index2][2] += norm[2] * area;

		normals[index3][0] += norm[0] * area;
		normals[index3][1] += norm[1] * area;
		normals[index3][2] += norm[2] * area;

	}
	std::cout << normals.size() << std::endl;
	for (int i = 0; i < normals.size(); i++)
	{
		float norm = sqrt(normals[i][0] * normals[i][0] + normals[i][1] * normals[i][1] + normals[i][2] * normals[i][2]);
		normals[i][0] /= norm;
		normals[i][1] /= norm;
		normals[i][2] /= norm;
		pointNormalsArray->SetTuple(i, normals[i].data());
	}
	polydata->GetPointData()->SetNormals(pointNormalsArray);
	polydata->Modified();
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1);

	std::cout << "Normal calculation took " << time_span.count() << " seconds." << std::endl;

}


