#include "colortableeditwidget.h"


ColorTableEditWidget::ColorTableEditWidget(QWidget *parent)
	: QWidget(parent)
{
	setAttribute(Qt::WA_StaticContents);
	color = vtkSmartPointer<vtkColorTransferFunction>::New();
	color->AddRGBPoint(0.0, 0.0, 0.0, 1.0);
	color->AddRGBPoint(0.1, 1.0, 0.0, 1.0);
	color->AddRGBPoint(0.2, 0.0, 1.0, 1.0);
	color->AddRGBPoint(0.3, 1.0, 1.0, 1.0);
	color->AddRGBPoint(0.4, 0.5, 0.0, 0.0);
	color->AddRGBPoint(0.5, 0.0, 0.5, 0.5);
	color->AddRGBPoint(0.6, 0.5, 0.5, 1.0);
	color->AddRGBPoint(0.7, 0.7, 0.3, 0.0);
	color->AddRGBPoint(0.8, 1.0, 0.7, 0.2);
	color->AddRGBPoint(0.9, 0.1, 0.8, 0.6);
	color->AddRGBPoint(1.0, 1.0, 0.0, 0.0);
}


bool ColorTableEditWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	update();
	return true;
}

void ColorTableEditWidget::GetInfo()
{
	std::cout << this->width() << " " << this->height() << std::endl;
}

vtkSmartPointer<vtkColorTransferFunction> ColorTableEditWidget::GetColorFunction()
{
	return color;
}

void ColorTableEditWidget::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPoint = event->pos();
	}
}

void ColorTableEditWidget::mouseDoubleClickEvent(QMouseEvent *event)
{

}

void ColorTableEditWidget::mouseMoveEvent(QMouseEvent *event)
{

}

void ColorTableEditWidget::mouseReleaseEvent(QMouseEvent *event)
{

}

void ColorTableEditWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void ColorTableEditWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
	this->resizeImage(image, QSize(this->width(), this->height()));
}

void ColorTableEditWidget::resizeImage(QImage &image, const QSize &newSize)
{
	if (image.size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), image);
	image = newImage;
	this->DrawScale();
	this->GetInfo();
}

void ColorTableEditWidget::DrawScale()
{
	int x = image.height();
	int y = image.width();
	double rgb[3];
	for (int i = 0; i < x; i++)
	{
		uchar* scanline = (uchar*)image.scanLine(i);
		for (int j = 0; j < y; j++)
		{
			color->GetColor(j/((double)y-1), rgb);
			scanline[j * 4 + 0] = (uchar)(rgb[2]*255);
			scanline[j * 4 + 1] = (uchar)(rgb[1]*255);
			scanline[j * 4 + 2] = (uchar)(rgb[0]*255);
		}
	}
	QPainter painter(&image);
	painter.setPen(QPen(Qt::red, 1));
	painter.setBrush(QBrush(Qt::red));
	y = image.height() / 2;
	for (int i = 0; i < color->GetSize(); i++)
	{
		x = color->GetDataPointer()[i * 4] * (image.width() - 1);
		painter.drawEllipse(QPoint(x, y), 5, 5);
	}
	painter.setPen(QPen(Qt::darkBlue, 1));
	painter.setBrush(QBrush(Qt::darkBlue));
	for (int i = 0; i < color->GetSize(); i++)
	{
		x = color->GetDataPointer()[i * 4] * (image.width() - 1);
		painter.drawEllipse(QPoint(x, y), 3, 3);
	}
	this->update();
}

